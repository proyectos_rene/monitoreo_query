package bo.com.bisa.bd;

import bo.com.bisa.seguridad.Utilidadesbase64;
import bo.com.bisa.util.Ctte;

import java.sql.*;

public class Conexion {

    protected Connection cn;

    public Conexion() throws Exception {
        Utilidadesbase64 cry = new Utilidadesbase64();
        try {
            //Class.forName("org.postgresql.Driver");
            DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
            cn = DriverManager.getConnection(Ctte.BDURL, Ctte.BDUSE, cry.Desencriptar(Ctte.BDPSW));
            Ctte.SUCCESS = true;
            System.out.println("success conexion BD.");
        } catch (Exception e) {
            Ctte.SUCCESS = false;
            throw new Exception("Se produjo un error al abrir la Conexion a la base de datos." + e.getMessage());
        }
    }

    public void cierraConexion() {
        try {
            if (cn != null) {
                cn.close();
            }
            cn = null;
        } catch (Exception e) {
            ;
        }
    }
}

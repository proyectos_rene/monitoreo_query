package bo.com.bisa.bd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Sql extends Conexion {
    public Sql() throws Exception {
        super();
    }

    public int query(String pQuery) throws Exception {
        int salida = 0;
        try {
            Statement st = null;
            ResultSet rs = null;
            st = cn.createStatement();
            rs = st.executeQuery(pQuery);
            if (!(rs == null || !rs.next())) {
                salida = rs.getInt(1);
            }
            if (st != null)
                st.close();
            if (rs != null)
                rs.close();
        } catch (SQLException e) {
            System.out.println("error de query: " + e.getMessage());
            throw new Exception(e.toString());
        }
        return salida;
    }

}
package bo.com.bisa.util;

import bo.com.bisa.bd.Sql;
import bo.com.bisa.seguridad.Utilidadesbase64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

public class Util {
    private FileWriter fichero = null;
    public static Logger LOGGER = Logger.getLogger(Correo.class);

    public void obtenerToken() throws Exception {
        Util util = new Util();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fechaHoy = sdf.format(new Date());
        try {
            File archivo = new File(Ctte.TXT);
            if (!archivo.exists()) {
                util.registrarPswArchivo(util.pedirPsw());
            } else {
                FileReader fr = new FileReader(Ctte.TXT);
                BufferedReader bf = new BufferedReader(fr);
                Ctte.FECHA = bf.readLine();
                Ctte.BDPSW = bf.readLine();
                if (Ctte.FECHA == null) {
                    util.registrarPswArchivo(util.pedirPsw());
                } else {
                    if (!fechaHoy.equals(Ctte.FECHA)) {
                        util.registrarPswArchivo(util.pedirPsw());
                    } else {
                        if (Ctte.BDPSW == null) {
                            util.registrarPswArchivo(util.pedirPsw());
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception("No se puede obtener el token." + e.getMessage());
        }
    }

    public void registrarPswArchivo(String pValue) throws Exception {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String fechaHoy = sdf.format(new Date());
            Utilidadesbase64 cry = new Utilidadesbase64();
            String vValueEncrip = cry.Encriptar(pValue);
            PrintWriter pw = null;
            fichero = new FileWriter(Ctte.TXT);
            pw = new PrintWriter(fichero);
            pw.println(fechaHoy);
            pw.println(vValueEncrip);
            Ctte.FECHA = fechaHoy;
            Ctte.BDPSW = vValueEncrip;
            fichero.close();
        } catch (Exception e) {
            throw new Exception("No pudo  grabar el archivo: " + e.getMessage());
        }
    }

    public String pedirPsw() {
        System.out.print("Por favor introduzca su contraseña de " + Ctte.BDUSE + ": ");
        Scanner entrada = new Scanner(System.in);
        String vCadena = entrada.nextLine();
        return vCadena;
    }


    public Document leerDocumentoXml() {
        Document doc = null;
        try {
            File file = new File(Ctte.XML);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);
            doc.getDocumentElement().normalize();
        } catch (Exception ex) {
            LOGGER.error("Error al leer documento xml:" + ex.getMessage());
        }
        return doc;
    }

    public void validarDocumentoXmlToXsd(Document doc) throws Exception {
        // Validar XSD
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new File(Ctte.XSD));
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(doc));
    }

    public void readProperties() throws IOException {
        // Leendo datos de la configuracion
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream(Ctte.PROPERTIES);
        prop.load(fis);
        Ctte.XML = prop.getProperty("monitoreo.query.xml");
        Ctte.XSD = prop.getProperty("monitoreo.query.xsd");
        Ctte.TXT = prop.getProperty("monitoreo.txt");
        Ctte.BDURL = prop.getProperty("url.conexion");
        fis.close();
    }

    public void saveLog(String asunto, String cuerpo, int pCantida) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaHoy = sdf.format(new Date());
        FileWriter archivo;
        if (new File(Ctte.FILE_LOG).exists() == false) {
            archivo = new FileWriter(new File(Ctte.FILE_LOG), false);
        }
        archivo = new FileWriter(new File(Ctte.FILE_LOG), true);
        archivo.write("[" + fechaHoy + "]" + "[INFO]: ASUNTO: " + asunto + "; SCRIP: " + cuerpo + "; CANTIDAD: " + pCantida + "\r\n");
        archivo.close();
    }

    public void abrirArchivoOpc1(String archivo) {
        try {
            File objetofile = new File(archivo);
            Desktop.getDesktop().open(objetofile);
        } catch (IOException ex) {
            LOGGER.error("Error al abrir el archivo:" + ex.getMessage());
        }
    }

    public void abrirArchivoOpc2(String archivo) {
        try {
            File file = new File(archivo);
            if (!Desktop.isDesktopSupported()) {
                System.out.println("not supported");
                return;
            }
            Desktop desktop = Desktop.getDesktop();
            if (file.exists())
                desktop.open(file);
        } catch (IOException ex) {
            LOGGER.error("Error al abrir el archivo:" + ex.getMessage());
        }
    }

    public boolean validarConexion() {
        Sql sql = null;
        try {
            sql = new Sql();
        } catch (Exception e) {
            System.out.println("Error en la conexión a la BD:" + e.getMessage());
            // LOGGER.error("Error en la conexión a la BD:" + e.getMessage());
        } finally {
            sql.cierraConexion();
        }

        return true;
    }
}

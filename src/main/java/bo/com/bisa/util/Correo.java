package bo.com.bisa.util;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

public class Correo {
    //public static Logger LOGGER = Logger.getLogger(Correo.class);
    public void enviarConGMail(String destinatario, String asunto, String cuerpo, int pCantidad) {
        String[] vDestinatarios = destinatario.split(";");
        //La dirección de correo de envío
        String remitente = "renergueta@gmail.com";
        //La clave de aplicación obtenida según se explica en este artículo:
        String claveemail = "vcndmcfaomssnkkw";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.setProperty("mail.smtp.clave", claveemail);    //La clave de la cuenta
        props.setProperty("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.setProperty("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google
        props.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
        props.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(remitente));

            Address[] destinos = new Address[vDestinatarios.length];//Aqui usamos el arreglo de destinatarios
            for (int i = 0; i < destinos.length; i++) {
                destinos[i] = new InternetAddress(vDestinatarios[i]);
            }
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   // Para un solo destinatario
            message.addRecipients(Message.RecipientType.TO, destinos);   // Para varios destinatarios
            message.setSubject(asunto);
            message.setText(cuerpo);
            String Msg =
                    "<html>" +
                            "<style type='text/css'>.e2 { color: #CCCCCC; font-family: tahoma; font-variant: small-caps; height: 35px; text-align: center; background-color: #0066CC; font-weight: bold; font-size: 12px; }" +
                            ".e3 { color: #003366; font-family: tahoma; text-align: left; font-size: 12px; } " +
                            "b { color: #113366; font-family: tahoma; text-align: left; font-size: 12px; font-weight: bold; }</style>" +
                            "<body><table width='100%' border='0' bgcolor='#E0EFFE' cellpadding='3' cellspacing='3'> " +
                            "<tr><td class='e2'>NOTIFICACION AUTOMATICA - MONITOREO QUERY BISA</td></tr> " +
                            "<tr><td class='e3'>Query: " + cuerpo + "</td></tr>" +
                            "<tr><td class='e3'>Cantidad de Registros Encontrados: " + pCantidad + "</td></tr>" +
                            "<tr><td class='e3'>Atte. Sistema de Monitoreo.</td></tr>" +
                            "</table></body></html>";

            message.setContent(Msg, "text/html");
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, claveemail);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            System.out.println("error al enviar correo:" + e.getMessage());
            //LOGGER.error("PROBLEMAS CON EL ENVIO DE CORREO:" + e.getMessage());
        }
    }
}

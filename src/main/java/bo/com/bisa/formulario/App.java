package bo.com.bisa.formulario;

import bo.com.bisa.query.Consulta;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App extends JFrame {
    private JButton btnProcesar;
    private JPanel panelMain;
    private JButton btnSalir;

    public App() {
        super("Query - Monitoreo");
        setContentPane(panelMain);
        btnSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                System.exit(0);
            }
        });
        btnProcesar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Consulta consulta = new Consulta();
                consulta.Consulta();
            }
        });
    }
}

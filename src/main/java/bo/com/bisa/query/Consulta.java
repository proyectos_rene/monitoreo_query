package bo.com.bisa.query;

import bo.com.bisa.bd.Sql;
import bo.com.bisa.formulario.App;
import bo.com.bisa.util.Correo;
import bo.com.bisa.util.Ctte;
import bo.com.bisa.util.Util;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import java.util.Timer;

import static bo.com.bisa.util.Ctte.FILE_LOG;

public class Consulta {
    public static String PEMAILS = "";
    public static String PSCRIPTS = "";
    public static String PASUNTO = "";
    public static String PENVIA_CORREO = "";
    public static Logger LOGGER = Logger.getLogger(Consulta.class);

    public static void main(String[] args) {
        System.out.println("*********** Inicio del programa ************");
        /*SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new App();
                frame.setSize(300, 300);
                frame.setVisible(true);
            }
        });*/
        Consulta consulta = new Consulta();
        consulta.Consulta();
        System.out.println("*********** Fin del programa ****************");
    }

    public void Consulta() {
        try {
            Util util = new Util();
            // Leer archivo de configuraciones
            util.readProperties();
            // Cargar documento xml
            Document doc = util.leerDocumentoXml();
            // Validar documento xml y xsd
            util.validarDocumentoXmlToXsd(doc);
            // Leer documento XML
            PEMAILS = GetTagXML(doc, "emails");
            PASUNTO = GetTagXML(doc, "asunto");
            PSCRIPTS = GetTagXML(doc, "script");
            PENVIA_CORREO = GetTagXML(doc, "envio-correo");
            int vFrecuenciaMin = GetTagXMLi(doc, "frecuencia-min");
            Ctte.BDUSE = GetTagXML(doc, "usuario");
            util.obtenerToken();
            try {
                util.validarConexion();
            } catch (Exception ex) {
                ;
            }
            int vCont = 0;
            while (!Ctte.SUCCESS && vCont < 2) {
                util.registrarPswArchivo(util.pedirPsw());
                try {
                    util.validarConexion();
                } catch (Exception ex) {
                    ;
                }
                if (vCont == 1) {
                    System.out.println("Error al conectarse a la Base de Datos, verifiqué su usuario y/o la cadena de conexión en: /etc/monitoreo/config.properties.");
                }
                vCont++;
            }
            if (Ctte.SUCCESS) {
                Tarea tarea = new Tarea();
                Timer temporizador = new Timer();
                temporizador.scheduleAtFixedRate(tarea, 0, 60000 * vFrecuenciaMin);
                //temporizador.scheduleAtFixedRate(tarea, 0, 10000 * vFrecuenciaMin);
            }
        } catch (Exception e) {
            //LOGGER.error("Problemas en el monitoreo:" + e.getMessage());
            System.out.println("Problemas en el monitoreo:" + e.getMessage());
        }
    }

    private String GetTagXML(Document doc, String tag) {
        try {
            NodeList listaNodosHijos = doc.getElementsByTagName(tag);
            return listaNodosHijos.item(0).getFirstChild().getNodeValue();
        } catch (Exception e) {
            return "";
        }
    }

    private int GetTagXMLi(Document doc, String tag) throws Exception {
        NodeList nodeLst = doc.getElementsByTagName(tag);
        Node nNode = nodeLst.item(0);
        return Integer.parseInt(nNode.getFirstChild().getTextContent());
    }

    public void procesoConsulta() {
        Sql sql = null;
        int vCant = 0;
        try {
            sql = new Sql();
            String[] vQuerys = PSCRIPTS.split(";");
            for (int i = 0; i < vQuerys.length; i++) {
                vCant = sql.query(vQuerys[i]);
                Util util = new Util();
                util.saveLog(PASUNTO, vQuerys[i].trim(), vCant);
                if ("S".equals(PENVIA_CORREO)) {
                    Correo correo = new Correo();
                    correo.enviarConGMail(PEMAILS, PASUNTO, vQuerys[i], vCant);
                }
            }
            if ("N".equals(PENVIA_CORREO)) {
                Util util = new Util();
                util.abrirArchivoOpc1(FILE_LOG);
            }
        } catch (Exception e) {
            //LOGGER.error("Error procesoConsulta:" + e.getMessage());
            System.out.println("Error procesoConsulta:" + e.getMessage());
        } finally {
            sql.cierraConexion();
        }
    }
}

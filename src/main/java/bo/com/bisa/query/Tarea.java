package bo.com.bisa.query;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author david
 */
public class Tarea extends TimerTask {
    static private final Logger LOGGER = Logger.getLogger("bo.com.bisa.Tarea");
    private Integer contador;

    public Tarea() {
        contador = 1;
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "Nro. de consulta {0}", contador);
        contador++;
        try {
            Consulta consulta = new Consulta();
            consulta.procesoConsulta();
        } catch (Exception e) {
            System.out.println("Error procesoConsulta:" + e.getMessage());
        }
    }
}